# build stage
FROM node:10-alpine as build-stage

WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY . .

RUN yarn build

# production stage
FROM nginx:alpine as production-stage

COPY ./nginx.conf /etc/nginx/nginx.conf

COPY --from=build-stage /app/dist /usr/share/nginx/html