const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

module.exports = {
    
    productionSourceMap: false,

    pwa: {    
        workboxPluginMode: 'InjectManifest',
        workboxOptions: {
            exclude: [
                /\.map$/,
                /manifest.*\.json$/,
                /^img\/.*/,
                /^file\/.*/
            ],
            swDest: `service-worker.js`,
            swSrc: './src/sw.js',
        }
    },

    chainWebpack: config => {

        const svgRule = config.module.rule('svg'); 
        svgRule.uses.clear();    
        svgRule
            .use('vue-svg-loader')
            .loader('vue-svg-loader');

        if (process.env.NODE_ENV === "production") {
            config.plugin('webpack-report')
                .use(BundleAnalyzerPlugin, [{
                    openAnalyzer: false,
                    analyzerMode: 'static'
                }]);
        }
    }
}