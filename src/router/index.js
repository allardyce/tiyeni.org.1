import Vue from 'vue';
import Router from 'vue-router';

import home from '@/views/home';
import method from '@/views/method';
import news from '@/views/news';
import about from '@/views/about';
import gallery from '@/views/gallery';
import people from '@/views/people';
import peopleDrilldown from '@/views/people-drilldown';
import contact from '@/views/contact';
import donate from '@/views/donate';
import videos from '@/views/videos';

import PrivacyPolicy from '@/views/privacy-policy';
import Error404 from '@/views/404';

import Newsletter2019June from '@/views/newsletters/2019-june';
import Newsletter2019April from '@/views/newsletters/2019-april';
import Newsletter2018October from '@/views/newsletters/2018-october';
import Newsletter2018July from '@/views/newsletters/2018-july';
import Newsletter2018May from '@/views/newsletters/2018-may';
import Newsletter2018February from '@/views/newsletters/2018-february';
import Newsletter2018January from '@/views/newsletters/2018-january';
import Newsletter2017September from '@/views/newsletters/2017-september';
import Newsletter2017August from '@/views/newsletters/2017-august';
import Newsletter2017June from '@/views/newsletters/2017-june';
import Newsletter2017May from '@/views/newsletters/2017-may';

Vue.use(Router);

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    linkActiveClass: 'is-active',
    routes: [
        { path: '/', meta: { name: 'home' }, component: home },
        { path: '/method', meta: { name: 'method' }, component: method },
        {
            path: '/news',
            meta: { name: 'news' },
            component: news,
            children: [
                { path: '2019/june', meta: { name: 'news', noscrollreset: true }, component: Newsletter2019June },
                { path: '2019/april', meta: { name: 'news', noscrollreset: true }, component: Newsletter2019April },
                { path: '2018/october', meta: { name: 'news', noscrollreset: true }, component: Newsletter2018October },
                { path: '2018/july', meta: { name: 'news', noscrollreset: true }, component: Newsletter2018July },
                { path: '2018/may', meta: { name: 'news', noscrollreset: true }, component: Newsletter2018May },
                { path: '2018/february', meta: { name: 'news', noscrollreset: true }, component: Newsletter2018February },
                { path: '2018/january', meta: { name: 'news', noscrollreset: true }, component: Newsletter2018January },
                { path: '2017/september', meta: { name: 'news', noscrollreset: true }, component: Newsletter2017September },
                { path: '2017/august', meta: { name: 'news', noscrollreset: true }, component: Newsletter2017August },
                { path: '2017/june', meta: { name: 'news', noscrollreset: true }, component: Newsletter2017June },
                { path: '2017/may', meta: { name: 'news', noscrollreset: true }, component: Newsletter2017May }
            ]
        },
        { path: '/gallery', meta: { name: 'gallery' }, component: gallery },
        { path: '/about', meta: { name: 'about' }, component: about },
        { path: '/about/people', meta: { name: 'people' }, component: people },
        { path: '/about/people/:person', meta: { name: 'people' }, component: peopleDrilldown },
        { path: '/contact', meta: { name: 'contact' }, component: contact },
        { path: '/donate', meta: { name: 'donate' }, component: donate },
        { path: '/videos', meta: { name: 'videos' }, component: videos },

        { path: '/privacy-policy', meta: { name: 'privacy-policy' }, component: PrivacyPolicy },

        { path: '*', meta: { name: '404' }, component: Error404 }
    ],
    scrollBehavior(to, from, savedPosition) {
        if (!to.meta.noscrollreset) {
            return { x: 0, y: 0 };
        }
    }
});
