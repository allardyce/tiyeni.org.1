workbox.core.setCacheNameDetails({ prefix: 'tiyeni.org' });

workbox.precaching.suppressWarnings();

self.__precacheManifest = [].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});

workbox.routing.registerRoute(
    new RegExp('/img/'),
    workbox.strategies.staleWhileRevalidate({
        cacheName: 'images',
        plugins: [
            new workbox.expiration.Plugin({
                maxEntries: 60,
                maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
            }),
        ],
    }),
);

workbox.routing.registerNavigationRoute('/index.html', {
    'blacklist': [
        new RegExp('/file/'),
    ],
});

addEventListener('message', event => {

    const replyPort = event.ports[0];
    const message = event.data;

    if (replyPort && message && message.type === 'skip-waiting') {

        event.waitUntil(
            self.skipWaiting().then(
                () => replyPort.postMessage({ precacheManifest: self.__precacheManifest, error: null }),
                error => replyPort.postMessage({ error })
            )
        )
        
    }

})