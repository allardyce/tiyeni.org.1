import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './registerServiceWorker'

import VueAnalytics from 'vue-analytics'
//Analytics for production
if (process.env.NODE_ENV === 'production') {
    Vue.use(VueAnalytics, { id: 'UA-117943290-1', router })
}

import './scripts/fontawesome'
import './scripts/parallax'
import './scripts/justify'
import './scripts/scroll'

import PhotoSwipe from './scripts/photoswipe'
Vue.use(PhotoSwipe)

import VueAgile from 'vue-agile'
Vue.use(VueAgile)

import logo from '@/assets/logo.svg';
Vue.component('logo', logo);

import './main.scss'

Vue.config.productionTip = false


const app = new Vue({
    router,
    render: h => h(App)
}).$mount('#app')

export { app }