export default [
    {
        title: 'Newsletter - June 2019',
        url: '/news/2019/june'
    },
    {
        title: 'Newsletter - April 2019',
        url: '/news/2019/april'
    },
    {
        title: 'Newsletter - October 2018',
        url: '/news/2018/october'
    },
    {
        title: 'Newsletter - July 2018',
        url: '/news/2018/july'
    },
    {
        title: 'Newsletter - May 2018',
        url: '/news/2018/may'
    },
    {
        title: 'Newsletter - February 2018',
        url: '/news/2018/february'
    },
    {
        title: 'Newsletter - January 2018',
        url: '/news/2018/january'
    },
    {
        title: 'Newsletter - September 2017',
        url: '/news/2017/september'
    },
    {
        title: 'Newsletter - August 2017',
        url: '/news/2017/august'
    },
    {
        title: 'Newsletter - June 2017',
        url: '/news/2017/june'
    },
    {
        title: 'Newsletter - May 2017',
        url: '/news/2017/may'
    }
];
