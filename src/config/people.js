export default {
    'isaac-monjo-chavula': {
        name: 'Isaac Monjo Chavula',
        title: 'Executive Director',
        picture: '/img/people/isaac-monjo-chavula-senior-manager-and-executive-director.jpg',
        description: `
            <p>Mr. Isaac Monjo Chavula (MSc., BSc., Dip. Agric.) is Executive Director of Tiyeni Malawi. He has over 20 years of working with international NGOs implementing food security, sustainable livelihoods, water and sanitation and natural management projects with Concern Universal (now the United Purpose) and the Feed the Future – INVC donor funding from USAID, EU, IrishAid, AusAid, and others. He has also lectured at the Natural Resources College (a constituent college of the Lilongwe University of Agriculture and Natural Resources) and the Malawi Adventist University as Adjunct lecturer and as resident Lecturer at the University of Lúrio in Moçambique for a total of 3 years. Prior to this, Isaac served as a civil servant in the Ministry of Trade and Industry and Ministry of Education, Science and Technology.</p>
            <hr>
            <p>MSc., BSc., Dip. Agriculture</p>
            <p>Mobile: <a href="tel:+265-999-207-400">+265-999-207-400</a>; Email: <a href="mailto:isaacchavulamonjo@yahoo.com">isaacchavulamonjo@yahoo.com</a>;</p>
            <h5 class="subtitle">Employment Record</h5>
            <ul>
                <li>Senior Manager/Executive Director, Tiyeni Malawi, 2 years.</li>
                <li>Lecturer, Universidade Lúrio, Moçambique and LUANAR-NRC, 3 years</li>
                <li>Agricultural Productivity Specialist & Technical Advisor, USAID/Malawi, 1 year.</li>
                <li>Livelihoods, Food Security & Sanitation Projects Manager, Concern Universal, 9 years.</li>
            </ul>
        `
    },
    'chance-mwenitete': {
        name: 'Chance Mwenitete',
        title: 'Finance & Legal Manager',
        picture: '/img/people/chance-mwenitete-finance-manager.jpg',
        description: `
            <p>Chance joined Tiyeni in February 2014 as a qualified accountant and works closely with Isaac, the team and the UK trustees to ensure that budgets are carefully planned and tightly controlled. Before he joined Tiyeni he was finance officer and then manager at Sambani Lodge on Lake Malawi. He is currently doing a University degree in his spare time.</p>
            <hr>
            <p>Position: Finance and Administration Manager.</p>
            <p>Mobile: +265-999-623-812 / +265-995-221-351</p>
            <p>Email: chancemwenitete@yahoo.com</p>
            <p>Has vast experience in finance management with Non-Governmental Organisations.</p>
        `
    },
    'namelord-priri': {
        name: 'Namelord Priri',
        title: 'Field Manager',
        picture: '/img/people/namelord-phiri-field-manager.jpg',
        description: `
            <p>Namelord comes from the Nkhata Bay district of Malawi, not far from Mzuzu, and was trained in agricultural practices joining Tiyeni as a field officer in 2013. He was agriculture development officer at the Ministry of Agriculture (1990-2010)  and  development facilitator with World Vision International (2010-12) and combines his experience of crop management and irrigation with the Tiyeni method of soil conservation and increased crop production.</p>
            <hr>
            <p>Diploma in Agriculture</p>
            <p>Contact No.: +265-995-221-345; Email: nabphiri@gmail.com</p>
            <p>Position with Tiyeni Malawi: Field Manager</p>
            <p>Responsibilities: Supervising Field Officers, Planning of day to day activities, Training Lead Farmers, Consolidation of field reports for compiling monthly reports.</p>
        `
    },
    'kumwenda-godfrey': {
        name: 'Kumwenda Godfrey',
        title: 'Training Manager',
        picture: '/img/people/godfrey-kumwenda-training-manager.jpg',
        description: `
            <p>Godfrey has been with Tiyeni since 2008 and is now planning and supervising field work in all the pods. He previously trained in functional landscape approach with Wetland Action in Malawi, and met the Crossleys while championing the establishment of an indigenous tree nursery with aid from The International Tree Foundation.</p>
            <hr>
            <p>Advanced Diploma in Community Development</p>
            <p>Contact no:  +265 (0) 884027456; Email: godfreykumwenda@yahoo.com</p>
            <p>Professional Qualification: MRCD degree: Pending</p>
            <p>Training Manager.2016. Responsible for planning and training Government extension workers and other organisations</p>
        `
    },
    'khalani-longwe': {
        name: 'Khalani Longwe',
        title: 'Field Officer',
        picture: '/img/people/khalani-longwe-field-officer.jpg',
        description: `
            <p>Khalani has had some training in agriculture and natural resources management and worked as a secondary school teacher for three years before becoming a Field Officer for Tiyeni.</p>
            <hr>
            <p>Mobile: +265-995-221-347 / +265-880-337-777</p>
            <p>Email: longwekhalani@gmail.com</p>
            <p>Position: Field Officer (Agriculture Extension).</p>
            <p>Responsibilities: Planning field activities with farmers; training farmers climate smart agriculture e.g. Deep Bed Farming; supervision of field activity implementation; monitoring and capturing data.</p>
        `
    },
    'love-msiska': {
        name: 'Love Msiska',
        title: 'Field Officer',
        picture: '/img/people/loveness-msiska-field-officer.jpg',
        description: `
            <p>Love studied agriculture and natural resources management at the Natural Resources College (NRC) and previously worked as Agriculture Extension Development Officer at Mpata Extension Planing Area (EPA) in Karonga before joining Tiyeni.</p>
            <hr>
            <p>Mobile: +265-995-221-349 / +265-882-936-291</p>
            <p>Email: lovemsiska@gmail.com</p>
            <p>Position: Field Officer (Agriculture Extension).</p>
            <p>Responsibilities: Planning field activities with farmers; training farmers climate smart agriculture e.g. Deep Bed Farming; providing technical support; monitoring and capturing data.</p>
        `
    },
    'timothy-moyo': {
        name: 'Timothy Moyo',
        title: 'Field Officer',
        picture: '/img/people/timothy-moyo-field-officer.jpg',
        description: `
            <p>Timothy studied agriculture and natural resources management at the Natural Resources College (NRC) and previously worked as an Extension Field Officer at Salima Rural Development Project before joining Tiyeni.</p>
            <hr>
            <p>Position: Field Officer (Agriculture Extension)</p>
            <p>Mobile: +265-995-221-346 / +265-885-281-723</p>
            <p>Email: timmoyo@gmail.com</p>
            <p>Responsibilities: Planning field activities with farmers; training farmers climate smart agriculture e.g. Deep Bed Farming; supervision of field activity implementation; monitoring and capturing data.</p>
        `
    },
    'france-gondwe': {
        name: 'France Gondwe',
        title: 'Monitoring and Evaluation Officer',
        picture: '/img/people/france-gondwe.jpg',
        description: `
            <p>France joined Tiyeni as a full-time Monitoring and Evaluation Officer in early 2018. France is highly qualified and has over 20 years experience of working with smallholder farmers.  He has done much work on the adoption of land conserving technologies, food security, sustainable livelihoods, profitability of smallholder agriculture and agricultural business advisory services. He also has over 10 years experience in agricultural research with multiple academic instiutions.</p>
            <hr>
            <p>Mobile: +265-888-875-374</p>
            <p>Email: fmtgondwe@yahoo.com</p>
            <p>Position: Field Officer (Agriculture Extension).</p>
            <p>Responsibilities: developing our tracking and data collection efforts.</p>
        `
    },
    'becky-kelly': {
        name: 'Becky Kelly',
        title: 'Fundraising and Communications',
        picture: '/img/people/becky-kelly.jpg',
        description: `
            <p>Becky joined Tiyeni in September 2015, holds a Masters Degree in Sustainable Development Advocacy and is experienced working in both the UK farming and International Development charity sectors. Part-time and UK based.</p>
            <hr>
            <p>Mobile: 07871 626673</p>
            <p>Email: thetiyenifund@gmail.com</p>
            <p>Position: Fundraising and Communications Coordinator</p>
            <p>Responsibilities: Deliver effective communications & develop fundraising streams in the UK.</p>
        `
    },
    'fabiano-chirwa': {
        name: 'Fabiano Chirwa',
        title: 'Field Officer',
        picture: '/img/people/fabiano-chirwa.jpg',
        description: `
            <p>Fabiano has a Diploma in Agriculture and joined Tiyeni at the start of 2018 on a temporary contract to run training in a new project area in Northern Malawi.</p>
            <hr>
            <p>Mobile: +265-881-107-703</p>
            <p>Email: fabianochirwa@gmail.com</p>
            <p>Position: Field Officer (Agriculture Extension).</p>
            <p>Responsibilities: Planning field activities with farmers; training farmers in climate smart agriculture (Deep Bed Farming); supervision of field activity implementation; monitoring and capturing data.</p>
        `
    }
};
