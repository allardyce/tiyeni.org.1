/* eslint-disable no-console */

import { register } from 'register-service-worker'
import { app } from './main'

if (process.env.NODE_ENV === 'production') {
  register(`${process.env.BASE_URL}service-worker.js`, {
    ready () {
      console.log(
        'App is being served from cache by a service worker.\n' +
        'For more details, visit https://goo.gl/AFskqB'
      )
    },
    registered (registration) {
      console.log('Service worker has been registered.')
    },
    cached (registration) {
      console.log('Content has been cached for offline use.')
      app.$emit('sw-cached');
    },
    updatefound (registration) {
      console.log('Content is downloading.')
      app.$emit('sw-updatefound', registration);
    },
    updated (registration) {
      console.log('New content is available; please refresh.')
      app.$emit('sw-updated', registration);
    },
    offline () {
      console.log('No internet connection found. App is running in offline mode.')
    },
    error (error) {
      console.error('Error during service worker registration:', error)
    }
  })
}
