import Vue from 'vue'
const basicScroll = require('basicscroll')


Vue.directive('parallax', {
    bind(el) {
        el.classList.add('parallax')
    },
    inserted(el, binding) {

        let img = el.querySelector('.parallax-img');

        img.onload = () => {
        
            basicScroll.create({
                elem: el,
                from: el.offsetTop < el.offsetHeight ? 'top-top' : 'top-bottom',
                to: 'bottom-top',
                direct: true,
                props: {
                    '--translateY': {
                        from: el.offsetHeight - img.offsetHeight + 'px',
                        to: 0
                    }
                }
            }).start()
        }
        
    }
})