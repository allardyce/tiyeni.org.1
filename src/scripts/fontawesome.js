import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'



import faEnvelope from '@fortawesome/fontawesome-free-regular/faEnvelope'
import faFileImage from '@fortawesome/fontawesome-free-regular/faFileImage'
import faNewspaper from '@fortawesome/fontawesome-free-regular/faNewspaper'

import faChevronCircleLeft from '@fortawesome/fontawesome-free-solid/faChevronCircleLeft'
import faChevronCircleRight from '@fortawesome/fontawesome-free-solid/faChevronCircleRight'
import faExclamation from '@fortawesome/fontawesome-free-solid/faExclamation'
import faWrench from '@fortawesome/fontawesome-free-solid/faWrench'
import faCheck from '@fortawesome/fontawesome-free-solid/faCheck'
import faFilePdf from '@fortawesome/fontawesome-free-solid/faFilePdf'

import faTwitter from '@fortawesome/fontawesome-free-brands/faTwitter'
import faFacebook from '@fortawesome/fontawesome-free-brands/faFacebook'
import faFlickr from '@fortawesome/fontawesome-free-brands/faFlickr'

library.add(faEnvelope, faNewspaper, faFileImage, faChevronCircleLeft, faChevronCircleRight, faExclamation, faWrench, faCheck, faFilePdf, faTwitter, faFacebook, faFlickr)

Vue.component('fa-icon', FontAwesomeIcon)