import PhotoSwipe from 'photoswipe/dist/photoswipe'
import PhotoSwipeDefaultUI from 'photoswipe/dist/photoswipe-ui-default'
import '@/styles/photoswipe.scss'

let $vm;

export default {
    install (Vue) {
        const PhotoSwipeComponent = Vue.component('photoswipe-viewer', {
            render: h => h('div',{staticClass:"pswp",attrs:{"tabindex":"-1","role":"dialog","aria-hidden":"true"}},[h('div',{staticClass:"pswp__bg"}),h('div',{staticClass:"pswp__scroll-wrap"},[h('div',{staticClass:"pswp__container"},[h('div',{staticClass:"pswp__item"}),h('div',{staticClass:"pswp__item"}),h('div',{staticClass:"pswp__item"})]),h('div',{staticClass:"pswp__ui pswp__ui--hidden"},[h('div',{staticClass:"pswp__top-bar"},[h('div',{staticClass:"pswp__counter"}),h('button',{staticClass:"pswp__button pswp__button--close",attrs:{"title":"Close (Esc)"}}),h('button',{staticClass:"pswp__button pswp__button--share",attrs:{"title":"Share"}}),h('button',{staticClass:"pswp__button pswp__button--fs",attrs:{"title":"Toggle fullscreen"}}),h('button',{staticClass:"pswp__button pswp__button--zoom",attrs:{"title":"Zoom in/out"}}),h('div',{staticClass:"pswp__preloader"},[h('div',{staticClass:"pswp__preloader__icn"},[h('div',{staticClass:"pswp__preloader__cut"},[h('div',{staticClass:"pswp__preloader__donut"})])])])]),h('div',{staticClass:"pswp__share-modal pswp__share-modal--hidden pswp__single-tap"},[h('div',{staticClass:"pswp__share-tooltip"})]),h('button',{staticClass:"pswp__button pswp__button--arrow--left",attrs:{"title":"Previous (arrow left)"}}),h('button',{staticClass:"pswp__button pswp__button--arrow--right",attrs:{"title":"Next (arrow right)"}}),h('div',{staticClass:"pswp__caption"},[h('div',{staticClass:"pswp__caption__center"})])])])]),
            methods: {
                open (index, items, el, options = { fullscreenEl: true, history: false, shareEl: true, tapToClose: true }) {
                    
                    const opts = Object.assign({
                        index: index,
                        getThumbBoundsFn(index) {
                            const el = document.querySelector(`#flickr-${index}`),
                                pageYScroll = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0,
                                rect = el.getBoundingClientRect();
                            return {
                                x: rect.left,
                                y: rect.top + pageYScroll,
                                w: rect.width
                            }
                        }
                    }, options);
                    
                    this.photoswipe = new PhotoSwipe(this.$el, PhotoSwipeDefaultUI, items, opts);
                    this.photoswipe.init();
                },
                close () {
                    this.photoswipe.close();
                }
            }
          })

        if (!$vm) {
            $vm = new PhotoSwipeComponent({el: document.createElement('div')});
            document.body.appendChild($vm.$el);
        }

        Vue.$photoswipe = {
            open (index, items, options) {
                $vm.open(index, items, options);
            },
            close () {
                $vm.close();
            }
        }

        Vue.mixin({
            created () {
                this.$photoswipe = Vue.$photoswipe;
            }
        })
    }
}