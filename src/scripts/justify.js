import Vue from 'vue'

(() => {

    Vue.directive('justify', {

        inserted(container, binding) {

            let height = screen.height * 0.25;

            container.classList.add('justify')

            let items = container.querySelectorAll('a')
            
            Array.prototype.forEach.call(items, item => {
                item.classList.add('justify-item')
                let img = item.querySelector('img')
                img.onload = () => {
                    item.style.width = height * img.naturalWidth / img.naturalHeight + 'px'
                    item.style.flexGrow = img.naturalWidth / img.naturalHeight
                }
            })

        }

    })

})()