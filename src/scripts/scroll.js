import 'intersection-observer'
import Vue from 'vue'

const observer = new IntersectionObserver((entries, observer) => {
    
    entries.forEach(entry => {
        if (entry.isIntersecting) {
            const el = entry.target
            el.classList.remove(el.dataset.scrollTransition)
            observer.unobserve(el)
        }
    })

}, { threshold: 0.1 });

Vue.directive('scroll', {
    inserted(el, binding) {

        let transition;
        if (binding.value && binding.value.transition) {
            transition = binding.value.transition
        } else {
            transition = 'fade';
        }
        
        el.classList.add('transisiton')
        el.classList.add(transition)

        if (binding.value && binding.value.delay) {
            el.style.transitionDelay = binding.value.delay;
        }

        el.dataset.scrollTransition = transition

        observer.observe(el)

    }
})